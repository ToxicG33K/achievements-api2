package rs.itcentar.achievements.api.data;

import java.util.UUID;

public class Achievement {
    private String id;
    private String displayName;
    private String description;
    private String icon;
    private int displayOrder;
    private long created;
    private long updated;

    public Achievement() {
    }

    //kad achievement stavljam u bazu treba mi konstr. bez id, displayOrder, created i updated - id se izgenerise u telu konstruktora, a o displayOrder, created i updated baza vodi racuna
    public Achievement(String displayName, String description, String icon) {
        this.id = UUID.randomUUID().toString();
        this.displayName = displayName;
        this.description = description;
        this.icon = icon;
    }

    
    //kad achievment vadim iz baze treba mi konstruktor sa svim poljima (i sa id, display order, created i updated)
    public Achievement(String id, String displayName, String description, String icon, int displayOrder, long created, long updated) {
        this.id = id;
        this.displayName = displayName;
        this.description = description;
        this.icon = icon;
        this.displayOrder = displayOrder;
        this.created = created;
        this.updated = updated;
    }

    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "Achievements{" + "id=" + id + ", displayName=" + displayName + ", description=" + description + ", icon=" + icon + ", displayOrder=" + displayOrder + ", created=" + created + ", updated=" + updated + '}';
    }
}
