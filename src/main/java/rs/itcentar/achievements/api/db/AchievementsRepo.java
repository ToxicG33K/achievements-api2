package rs.itcentar.achievements.api.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import rs.itcentar.achievements.api.data.Achievement;

public class AchievementsRepo implements Repository<Achievement> {

    private final DBConnection dbConnect = new DBConnection();

    private Connection connect() {
        Optional<Connection> optConnection = dbConnect.getConnection();
        return optConnection.orElseThrow(() -> {
            return new RuntimeException("Connecting to database failed!");
        });
    }

    @Override
    public Optional<Achievement> findOne(String id) {
        try (Connection db = connect()) {
            PreparedStatement ps = db.prepareStatement("SELECT * FROM achievements WHERE id = ? LIMIT 1");
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();

            id = rs.getString("id");
            String displayName = rs.getString("displayName");
            String description = rs.getString("description");
            String icon = rs.getString("icon");
            int displayOrder = rs.getInt("displayOrder");
            long created = rs.getLong("created");
            long updated = rs.getLong("updated");
            return Optional.of(new Achievement(id, displayName, description, icon,
                    displayOrder, created, updated));
        } catch (SQLException ex) {
            Logger.getLogger(AchievementsRepo.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Optional.empty();
    }

    @Override
    public List<Achievement> findAll() {
        List<Achievement> list = new ArrayList<>();
        try (Connection db = connect()) {
            PreparedStatement ps = db.prepareStatement("SELECT * FROM achievements");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String id = rs.getString("id");
                String displayName = rs.getString("displayName");
                String description = rs.getString("description");
                String icon = rs.getString("icon");
                int displayOrder = rs.getInt("displayOrder");
                long created = rs.getLong("created");
                long updated = rs.getLong("updated");
                list.add(new Achievement(id, displayName, description, icon,
                        displayOrder, created, updated));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AchievementsRepo.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public Optional<Achievement> save(Achievement t) {
        try (Connection db = connect()) {
            PreparedStatement ps = db.prepareStatement("INSERT INTO achievements (id, displayName, description, icon, displayOrder, created, updated) VALUES (?, ?, ?, ?, ?, ?, ?)");

            t.setId(UUID.randomUUID().toString());
            t.setCreated(System.currentTimeMillis());
            t.setUpdated(0);

            ps.setString(1, t.getId());
            ps.setString(2, t.getDisplayName());
            ps.setString(3, t.getDescription());
            ps.setString(4, t.getIcon());
            ps.setInt(5, t.getDisplayOrder());
            ps.setLong(6, t.getCreated());
            ps.setLong(7, t.getUpdated());
            int ret = ps.executeUpdate();
            if (ret == 1) {
                return Optional.of(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AchievementsRepo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Achievement> delete(String id) {
        try (Connection db = connect()) {
            Optional<Achievement> t = findOne(id);
            PreparedStatement ps = db.prepareStatement("DELETE FROM achievements WHERE id = ?");
            ps.setString(1, id);
            int ret = ps.executeUpdate();
            if (ret == 1) {
                return t;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AchievementsRepo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Achievement> update(String id, Achievement t) {
        try (Connection db = connect()) {
            PreparedStatement ps = db.prepareStatement("UPDATE achievements SET displayName = ?, description = ?, Icon = ?, displayOrder = ?, created = ?, updated = ? WHERE id = ?");
            ps.setString(1, t.getDisplayName());
            ps.setString(2, t.getDescription());
            ps.setString(3, t.getIcon());
            ps.setInt(4, t.getDisplayOrder());
            ps.setLong(5, t.getCreated());
            ps.setLong(6, t.getUpdated());
            ps.setString(7, id);
            int ret = ps.executeUpdate();
            if (ret == 1) {
                t.setId(id);
                return Optional.of(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AchievementsRepo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    @Override
    public List<Achievement> getAchievements(String gameId) {
        List<Achievement> list = new ArrayList<>();
        try (Connection db = connect()) {
            PreparedStatement ps = db.prepareStatement("SELECT achievements.*, games.gameDisplayName FROM achievements\n"
                    + "INNER JOIN games_has_achievements ON games_has_achievements.achievements_id=achievements.id\n"
                    + "INNER JOIN games ON games_has_achievements.games_id=games.id\n"
                    + "WHERE games.id=?\n"
                    + "ORDER BY displayOrder ASC");
            ps.setString(1, gameId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String id = rs.getString("id");
                String displayName = rs.getString("displayName");
                String description = rs.getString("description");
                String icon = rs.getString("icon");
                int displayOrder = rs.getInt("displayOrder");
                long created = rs.getLong("created");
                long updated = rs.getLong("updated");
                list.add(new Achievement(id, displayName, description, icon,
                        displayOrder, created, updated));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AchievementsRepo.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

}
