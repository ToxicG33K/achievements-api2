package rs.itcentar.achievements.api.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {
    private final DBConfig config = new DBConfig();
    
    public Optional<Connection> getConnection() {
        try {
            Class.forName(config.getJDBCDriver());
            Connection connection = DriverManager.getConnection(config.getJDBCUrl(), config.getJDBCUser(), config.getJDBCPassword());
            return Optional.of(connection);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Optional.empty();
    }
}
